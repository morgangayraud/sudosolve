package com.applications.tbrain.sudosolve;

import android.app.Activity;
import android.app.LauncherActivity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.applications.tbrain.sudosolve.grid.ListItem;
import com.applications.tbrain.sudosolve.grid.TextAdapter;
import com.applications.tbrain.sudosolve.opencv.OpenCvActivity;
import com.applications.tbrain.sudosolve.solver.Solver;

import org.opencv.android.OpenCVLoader;

import java.util.ArrayList;


public class MainActivity extends Activity {

    Button button;
    Button button2;
    GridView gridView;
    TextAdapter textAdapter;

    MainActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.instance = this;

        addListenerOnButton();

        gridView = (GridView) findViewById(R.id.gridView1);
        textAdapter = new TextAdapter(this);
        gridView.setAdapter(textAdapter);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addListenerOnButton() {

        button = (Button) findViewById(R.id.button1);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                Solver solver = new Solver();
                int[][] matrice = getValues();
                try {
                    applySolve(solver.solve(matrice));
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(),
                            e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }

            }

        });

        button2 = (Button) findViewById(R.id.button2);

        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                //System.loadLibrary("hello");

                //System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
                Intent openCvIntent =
                        new Intent(instance, OpenCvActivity.class);
                startActivity(openCvIntent);

            }

        });

    }

    private int[][] getValues() {
        int[][] matrice = new int[9][9];

        gridView = (GridView) findViewById(R.id.gridView1);

        for (int h=0;h<81;h++) {
            ListItem item = (ListItem)gridView.getAdapter().getItem(h);
            if (item.caption.length()>0) {
                int i = h/9;
                int j = h%9 +1;
                matrice[i][j-1] = Integer.parseInt(item.caption);
            }
        }
        return matrice;
    }

    private void applySolve(int[][] matrice) {
        int h = 0;
        gridView = (GridView) findViewById(R.id.gridView1);

        for (int i=0 ; i < 9 ; i++) {
            for (int j = 0 ; j < 9 ; j++) {
                ListItem item = (ListItem)gridView.getAdapter().getItem(h);
                item.caption = String.valueOf(matrice[i][j]);
                h++;
            }
        }
        ((TextAdapter)gridView.getAdapter()).notifyDataSetChanged();
    }

public native String getStringValue();


    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            gridView = (GridView) findViewById(R.id.gridView1);
           // ((TextAdapter)gridView.getAdapter()).holdFocus();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }*/
}
