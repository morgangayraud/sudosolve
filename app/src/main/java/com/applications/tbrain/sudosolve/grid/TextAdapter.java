package com.applications.tbrain.sudosolve.grid;


import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.applications.tbrain.sudosolve.R;

/**
 * Created by morgan on 25/08/2014.
 */
public class TextAdapter extends BaseAdapter{
    private LayoutInflater mInflater;
    public ArrayList myItems = new ArrayList();
    private int position = 0;
    private boolean hasFocused = false;

    public TextAdapter(Activity activity) {
        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < 81; i++) {
            ListItem listItem = new ListItem();
            listItem.caption = "";
            myItems.add(listItem);
        }
        notifyDataSetChanged();
    }

    /*
    @Override
    public void notifyDataSetChanged() {
        for (int i = 0; i < 81; i++) {
            ((ListItem) myItems.get(TextAdapter.this.position)).caption = s.toString();
        }
        super.notifyDataSetChanged();
    }*/


    public int getCount() {
        return myItems.size();
    }

    public Object getItem(int position) {
        return myItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.row_grid, null);
            holder.caption = (EditText) convertView
                    .findViewById(R.id.ItemCaption);
            holder.caption.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable s) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (TextAdapter.this.hasFocused && (s.length() > 0
                    || (((ListItem) myItems.get(TextAdapter.this.position)).caption.length()>0 && s.length()==0))) {
                        ((ListItem) myItems.get(TextAdapter.this.position)).caption = s.toString();
                        TextAdapter.this.hasFocused = false;
                    }
                }
            });
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //Fill EditText with the value you have in data source
 //       holder.caption.setText(((ListItem)myItems.get(position)).caption);
        holder.caption.setId(position);

        //we need to update adapter once we finish with editing
        holder.caption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                TextAdapter.this.position = v.getId();
                if (hasFocus) {
                    TextAdapter.this.hasFocused = true;
                }
                else {
                    TextAdapter.this.hasFocused = false;
                }
            }
        });

        return convertView;
    }

    public void holdFocus() {
        this.hasFocused = false;
    }

}


class ViewHolder {
    EditText caption;
}
