package com.applications.tbrain.sudosolve.solver;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by morgan on 24/08/2014.
 */
public class Solver {
    private static int inconnues;

    public void testSolve(String[] args) throws Exception {
        int[][] matrice = new int[9][9];

        initHardestEver(matrice);

        showMatrice(matrice);

        int[][] solution = solve(matrice);
        System.out.println("    ");
        System.out.println("Solution:");
        System.out.println("    ");
        showMatrice(solution);

    }

    public int[][] solve(int[][] matrice) throws Exception {
        inconnues = 81;
        // mat poss
        Set<?>[][] matricePoss = new HashSet<?>[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                matricePoss[i][j] = init0to9();
            }
        }
        // init inconnues
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (matrice[i][j] > 0) {
                    inconnues--;
                    matricePoss[i][j].clear();
                }
            }
        }
        checkCorrect(matrice);
        while (inconnues > 0) {
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    filtrerSquare(matrice, matricePoss, i, j);
                }
            }
            for (int i = 0; i < 9; i++) {
                filtrerLigne(matrice, matricePoss, i);
            }
            for (int j = 0; j < 9; j++) {
                filtrerColonne(matrice, matricePoss, j);
            }
            checkCoherence(matrice,matricePoss);
            checkCorrect(matrice);
            if (!resoudreUniquePoss(matrice, matricePoss)) {
                boolean trouve = false;
                // méthode find sol unique
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        if (findSquare(matrice, matricePoss, i, j)) {
                            trouve = true;
                        }
                    }
                }
                for (int i = 0; i < 9; i++) {
                    if (findLigne(matrice, matricePoss, i)) {
                        trouve = true;
                    }
                }
                for (int j = 0; j < 9; j++) {
                    if (findColonne(matrice, matricePoss, j)) {
                        trouve = true;
                    }
                }

                if (!trouve) {
                    // méthode brute

                    for (int i = 0; i < 9; i++) {
                        for (int j = 0; j < 9; j++) {
                            if (matricePoss[i][j].size() == 2) {
                                int[][] matriceMem = clone(matrice);
                                try {
                                    matrice[i][j] = (Integer) matricePoss[i][j]
                                            .toArray()[0];
                                    return solve(matrice);
                                } catch (Exception ex) {
                                    matriceMem[i][j] = (Integer) matricePoss[i][j]
                                            .toArray()[1];
                                    return solve(matriceMem);
                                }
                            }
                        }
                    }

                    throw new Exception("matrice trop complexe");
                }
            }
        }

        return matrice;
    }

    // clone de matrice
    private int[][] clone(int[][] matrice) {
        int[][] matriceMem = new int[9][9];
        for (int i = 0;i<9;i++)
        {
            matriceMem[i] = matrice[i].clone();
        }
        return matriceMem;
    }

    // vérifie si la matrice est toujours correcte
    private void checkCorrect(int[][] matrice) throws Exception {
        // Squares
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int[] nbs = new int[10];
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {
                        nbs[matrice[i * 3 + k][j * 3 + l]]++;
                    }
                }
                for (int k = 1; k < 10; k++) {
                    if(nbs[k]>1)
                        throw new Exception("pas de solution");
                }
            }
        }

        // lignes
        for (int i = 0; i < 9; i++) {
            int[] nbs = new int[10];
            for (int j = 0; j < 9; j++) {
                nbs[matrice[i][j]]++;
            }
            for (int k = 1; k < 10; k++) {
                if(nbs[k]>1)
                    throw new Exception("pas de solution");
            }
        }

        // colones
        for (int j = 0; j < 9; j++) {
            int[] nbs = new int[10];
            for (int i = 0; i < 9; i++) {
                nbs[matrice[i][j]]++;
            }
            for (int k = 1; k < 10; k++) {
                if(nbs[k]>1)
                    throw new Exception("pas de solution");
            }
        }
    }

    private boolean findColonne(int[][] matrice, Set<?>[][] matricePoss,
                                       int j) throws Exception {
        boolean trouve = false;
        // liste des elmts absents de la colonne
        Set<Integer> elmtAbsent = init0to9();
        for (int i = 0; i < 9; i++) {
            elmtAbsent.remove(matrice[i][j]);
        }
        // cherche les elmts à une occurence possible
        for (int chiffre : elmtAbsent) {
            int nbOccur = 0;
            for (int i = 0; i < 9; i++) {
                if (matricePoss[i][j].contains(chiffre)) {
                    nbOccur++;
                }
            }
            if (nbOccur == 1) {
                for (int i = 0; i < 9; i++) {
                    if (matricePoss[i][j].contains(chiffre)) {
                        matrice[i][j] = chiffre;
                        matricePoss[i][j].clear();
                        inconnues--;
                        trouve = true;
                        checkCorrect(matrice);
                    }
                }
            }
        }
        return trouve;
    }

    private boolean findLigne(int[][] matrice, Set<?>[][] matricePoss,
                                     int i) throws Exception {
        boolean trouve = false;
        // liste des elmts absents de la ligne
        Set<Integer> elmtAbsent = init0to9();
        for (int j = 0; j < 9; j++) {
            elmtAbsent.remove(matrice[i][j]);
        }
        // cherche les elmts à une occurence possible
        for (int chiffre : elmtAbsent) {
            int nbOccur = 0;
            for (int j = 0; j < 9; j++) {
                if (matricePoss[i][j].contains(chiffre)) {
                    nbOccur++;
                }
            }
            if (nbOccur == 1) {
                for (int j = 0; j < 9; j++) {
                    if (matricePoss[i][j].contains(chiffre)) {
                        matrice[i][j] = chiffre;
                        matricePoss[i][j].clear();
                        inconnues--;
                        trouve = true;
                        checkCorrect(matrice);
                    }
                }
            }
        }
        return trouve;
    }

    // trouve les elmts à une occurence possible ds un square
    private boolean findSquare(int[][] matrice, Set<?>[][] matricePoss,
                                      int i, int j) throws Exception {
        boolean trouve = false;
        // liste des elmts absents du square
        Set<Integer> elmtAbsent = init0to9();
        for (int k = 0; k < 3; k++) {
            for (int l = 0; l < 3; l++) {
                elmtAbsent.remove(matrice[i * 3 + k][j * 3 + l]);
            }
        }
        // cherche les elmts à une occurence possible
        for (int chiffre : elmtAbsent) {
            int nbOccur = 0;
            for (int k = 0; k < 3; k++) {
                for (int l = 0; l < 3; l++) {
                    if (matricePoss[i * 3 + k][j * 3 + l].contains(chiffre)) {
                        nbOccur++;
                    }
                }
            }
            if (nbOccur == 1) {
                for (int k = 0; k < 3; k++) {
                    for (int l = 0; l < 3; l++) {
                        if (matricePoss[i * 3 + k][j * 3 + l].contains(chiffre)) {
                            matrice[i * 3 + k][j * 3 + l] = chiffre;
                            matricePoss[i * 3 + k][j * 3 + l].clear();
                            inconnues--;
                            trouve = true;
                            checkCorrect(matrice);
                        }
                    }
                }
            }
        }
        return trouve;
    }

    // vide les poss des elmts d'une colonne
    private void filtrerColonne(int[][] matrice, Set<?>[][] matricePoss,
                                       int j) {
        // liste des elmts présent dans le square
        Set<Integer> elmtPresent = new HashSet<Integer>();
        for (int i = 0; i < 9; i++) {
            elmtPresent.add(matrice[i][j]);
        }
        // vide les elmts présents
        for (int i = 0; i < 9; i++) {
            matricePoss[i][j].removeAll(elmtPresent);
        }

    }

    // vide les poss des elmts d'une ligne
    private void filtrerLigne(int[][] matrice, Set<?>[][] matricePoss,
                                     int i) {
        // liste des elmts présent dans le square
        Set<Integer> elmtPresent = new HashSet<Integer>();
        for (int j = 0; j < 9; j++) {
            elmtPresent.add(matrice[i][j]);
        }
        // vide les elmts présents
        for (int j = 0; j < 9; j++) {
            matricePoss[i][j].removeAll(elmtPresent);
        }
    }

    // vide les poss des elmts du square
    private static void filtrerSquare(int[][] matrice, Set<?>[][] matricePoss,
                                      int i, int j) {
        // liste des elmts présent dans le square
        Set<Integer> elmtPresent = new HashSet<Integer>();
        for (int k = 0; k < 3; k++) {
            for (int l = 0; l < 3; l++) {
                elmtPresent.add(matrice[i * 3 + k][j * 3 + l]);
            }
        }
        // vide les elmts présents
        for (int k = 0; k < 3; k++) {
            for (int l = 0; l < 3; l++) {
                matricePoss[i * 3 + k][j * 3 + l].removeAll(elmtPresent);
            }
        }
    }

    // vérifie la cohérence entre matrice et matricePoss
    private static void checkCoherence(int[][] matrice, Set<?>[][] matricePoss) throws Exception {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if(matrice[i][j]==0 && matricePoss[i][j].size()==0)
                    throw new Exception("incohérence");
            }
        }
    }

    // parcours matPoss -> si poss unique ajout à matrice
    private boolean resoudreUniquePoss(int[][] matrice,
                                              Set<?>[][] matricePoss) throws Exception {
        boolean trouve = false;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (matricePoss[i][j].size() == 1) {
                    trouve = true;
                    inconnues--;
                    matrice[i][j] = (Integer) matricePoss[i][j].toArray()[0];
                    matricePoss[i][j].clear();
                    checkCorrect(matrice);
                }
            }
        }
        return trouve;
    }

    private Set<Integer> init0to9() {
        Set<Integer> liste = new HashSet<Integer>();
        for (int i = 1; i < 10; i++) {
            liste.add(i);
        }
        return liste;
    }

    private void showMatrice(int[][] matrice) {
        for (int i = 0; i < 9; i++) {
            System.out.print("|");
            for (int j = 0; j < 9; j++) {
                System.out.print(matrice[i][j] + "|");
            }
            System.out.print("\n");
        }

    }

    private void initHardestEver(int[][] matrice) {
        matrice[0][0] = 8;
        matrice[1][2] = 3;
        matrice[1][3] = 6;
        matrice[2][1] = 7;
        matrice[2][4] = 9;
        matrice[2][6] = 2;
        matrice[3][1] = 5;
        matrice[3][5] = 7;
        matrice[4][4] = 4;
        matrice[4][5] = 5;
        matrice[4][6] = 7;
        matrice[5][3] = 1;
        matrice[5][7] = 3;
        matrice[6][2] = 1;
        matrice[6][7] = 6;
        matrice[6][8] = 8;
        matrice[7][2] = 8;
        matrice[7][3] = 5;
        matrice[7][7] = 1;
        matrice[8][1] = 9;
        matrice[8][6] = 4;
    }

}
